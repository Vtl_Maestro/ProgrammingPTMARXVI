package ua.org.oa.miniaylov.practical1;

/**
 * Created by Vitaliy on 20.06.2016.
 */
public class ArrayProd {

    private double [] array;

    public  ArrayProd(double [] array){
        this.array = array;
    }

    public double prod(){
        double result=1;
        for (double element : array) {

            result *= element;
        }
        return result;
    }
}
