package ua.org.oa.miniaylov.practical1;

public class ArraySum {

    public static int sum(int[] inArray){
        int result = 0;
        for (int i = 0; i < inArray.length; i++) {
            int inArr =  inArray[i];
            result += inArr;
        }
        return result;
    }
}