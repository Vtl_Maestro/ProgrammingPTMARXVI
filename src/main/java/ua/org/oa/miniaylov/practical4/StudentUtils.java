package ua.org.oa.miniaylov.practical4;

import java.util.*;

public class StudentUtils {


    public static Map<String, Student> createMapFromList(List<Student> students) {
        String key;
        HashMap<String, Student> stringStudentHashMap = new HashMap<>();
        for (Student student : students) {
            key = student.getFirstName() + student.getLastName();
            stringStudentHashMap.put(key, student);
        }

        for (Map.Entry entry : stringStudentHashMap.entrySet()) {
            System.out.println(entry);
        }
        return stringStudentHashMap;

    }

    public static void printStudents(List<Student> students, int course) {

        for (Student student : students) {
            if (course == student.getCourse())
                System.out.println(student);
        }

    }

    public List<Student> sortStudent(List students) {
        students.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                int i = o1.getFirstName().compareTo(o2.getFirstName());
                return i;
            }
        });
        students.forEach(System.out::println);
        return students;
    }
}
