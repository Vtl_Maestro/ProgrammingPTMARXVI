package ua.org.oa.miniaylov.practical4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ua.org.oa.miniaylov.practical4.StringUtils.MySort.DOWNKEY;
import static ua.org.oa.miniaylov.practical4.StringUtils.MySort.UPPERKEY;

public class App {

    public static void main(String[] args) {

        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student("Vasya", "Sidorov", 1));
        studentList.add(new Student("Petr", "Petrov", 5));
        studentList.add(new Student("Sergey", "Kolobov", 3));
        studentList.add(new Student("Boris", "Chaluy", 3));

        StudentUtils studentUtils = new StudentUtils();

        System.out.println("=============================createMapFromList===========================");
        studentUtils.createMapFromList(studentList);
        System.out.println("==============================printStudents==========================");
        studentUtils.printStudents(studentList, 3);
        System.out.println("===========================sortStudent=============================");
        studentUtils.sortStudent(studentList);


        System.out.println("===========================countWorld=============================");
        StringUtils stringUtils = new StringUtils();
        try {
            stringUtils.countWorld();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("===========================countWorld(UPPERKEY)=============================");
        stringUtils.countWorld(UPPERKEY);

        System.out.println("===========================countWorld(DOWNKEY)=============================");
        stringUtils.countWorld(DOWNKEY);
    }
}
