package ua.org.oa.miniaylov.practical6_1;

/**
 * Created by Vitaliy on 30.05.2016.
 */
public class MyThread extends Thread{

    public MyThread(String name) {
        super(name);
    }
    @Override
    public void run() {
        for (int i = 10; i > 0; i--)
            try {
                System.out.println("Name myThread: " + currentThread());
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


