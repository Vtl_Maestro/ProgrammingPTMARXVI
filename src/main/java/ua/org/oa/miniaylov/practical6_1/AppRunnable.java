package ua.org.oa.miniaylov.practical6_1;

/**
 * Created by Vitaliy on 31.05.2016.
 */
public class AppRunnable {

    public static void main(String[] args) {
        Thread runnableThread = new Thread(new MyRunnable());
        runnableThread.start();
    }
}
