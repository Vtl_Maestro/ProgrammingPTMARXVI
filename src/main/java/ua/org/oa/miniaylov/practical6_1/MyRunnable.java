package ua.org.oa.miniaylov.practical6_1;

import static java.lang.Thread.currentThread;

/**
 * Created by Vitaliy on 31.05.2016.
 */
public class MyRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 10; i > 0; i--)
            try {
                System.out.println("Name myThread: " + currentThread());
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}

