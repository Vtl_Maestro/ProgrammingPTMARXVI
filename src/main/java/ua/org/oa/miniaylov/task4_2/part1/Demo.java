package ua.org.oa.miniaylov.task4_2.part1;

public class Demo {

    public static void main(String[] args) {

        MyDeque<Number> deque = new MyDequeImpl<Number>();

        deque.addFirst(111);
        deque.addLast(222);
        deque.addLast(333);
        deque.addLast(444);
        deque.addLast(555);
        System.out.println("==================toArray()===================");
        deque.toArray();
        System.out.println("================removeFirst()=================");
        deque.removeFirst();
        deque.toArray();
        System.out.println("=================removeLast()=================");
        deque.removeLast();
        deque.toArray();
        System.out.println("==================getFirst()==================");
        System.out.println(deque.getFirst());
        System.out.println("==================getLast()===================");
        System.out.println(deque.getLast());
        System.out.println("list contains 333 --> " + deque.contains(333));
        System.out.println("size = " + deque.size());
        System.out.println("containsAll = " + deque.containsAll(deque));
        System.out.println("==================clear()===================");
        deque.clear();
        System.out.println("clear = " + deque.toArray());
    }
}
