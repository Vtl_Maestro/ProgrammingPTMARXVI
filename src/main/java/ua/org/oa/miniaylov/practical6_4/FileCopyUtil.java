package ua.org.oa.miniaylov.practical6_4;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Vitaliy on 01.06.2016.
 */
public class FileCopyUtil {

    public void copyFiles(String fileName, String fileNewPath, String dirNewPath, String dir) {
        File file = new File(dir);
        File[] fileNameArr = file.listFiles();
        String stringNewDir = null;
        for (File fl : fileNameArr) {
            if (fl.isFile()) {
                String stringNameFile = null;
                try {
                    stringNameFile = fl.getCanonicalPath();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (stringNameFile.contains(fileName)) {
                    BufferedReader bufferedReader = null;
                    PrintWriter printWriter = null;
                        try{
                                bufferedReader = new BufferedReader(new FileReader(stringNameFile));
                                printWriter = new PrintWriter(new FileWriter(fileNewPath, true));
                            String s;
                            while ((s = bufferedReader.readLine()) != null){
                                printWriter.println(s);
                            }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        finally {
                            if(bufferedReader != null){
                                try {
                                    bufferedReader.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (printWriter != null){
                                printWriter.close();
                            }
                        }
                     }
                }
            if (fl.isDirectory()) {
                Path direct = Paths.get(dirNewPath);
                try {
                    Files.createDirectory(direct);
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("Папка с таким именем уже существует!!!");
                }
                try {
                    stringNewDir = fl.getCanonicalPath();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final String finalStringNameDir = stringNewDir;
                new Thread() {
                    @Override
                    public void run() {
                        copyFiles(fileName, fileNewPath, dirNewPath, finalStringNameDir);
                    }
                }.start();
              }
            }
        }
    }

