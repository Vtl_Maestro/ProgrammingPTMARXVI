package ua.org.oa.miniaylov.practical6_3;

import java.io.*;

/**
 * Created by Vitaliy on 31.05.2016.
 */
public class ClassFileUtil {

    public void printNameFiles(String fileName, String logPath, String dir) {
        File file = new File(dir);
        File[] fileNameArr = file.listFiles();
        String stringNameDir = null;
        for (File fl : fileNameArr) {
            if (fl.isDirectory()) {
                try {
                    stringNameDir = fl.getCanonicalPath();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final String finalStringNameDir = stringNameDir;
                new Thread() {
                    @Override
                    public void run() {
                        printNameFiles(fileName, logPath, finalStringNameDir);
                    }
                }.start();
            }
                if (fl.isFile()) {
                    String stringNameFile = null;
                    try {
                        stringNameFile = fl.getCanonicalPath();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (stringNameFile.contains(fileName)) {
                        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter
                                (new FileOutputStream(logPath, true)))) {
                            bw.write(stringNameFile);
                            bw.newLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

