package ua.org.oa.miniaylov.task3;

public class Shape<T,S> {
    private T tipe;
    private S yardage;

    public Shape(T tipe, S yardage) {
        setTipe(tipe);
        setYardage(yardage);
    }

    public Shape(T tipe){
        setTipe(tipe);
    }

    public T getTipe() {
        return tipe;
    }

    public void setTipe(T tipe) {
        this.tipe = tipe;
    }

    public S getYardage() {
        return yardage;
    }

    public void setYardage(S yardage) {
        this.yardage = yardage;
    }

    @Override
    public String toString() {
        return "Shape{" +
                "tipe=" + tipe +
                ", yardage=" + yardage +
                '}';
    }
}
