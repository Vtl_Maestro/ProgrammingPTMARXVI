package ua.org.oa.miniaylov.task3;

public class App {

    public static void main(String[] args) {
        GenericStorage<String, Shape<String,Integer>> shapeStorage = new GenericStorage();
        //Метод add(T obj), который добавит новый элемент в хранилище в конец:
        shapeStorage.add(new Shape("Triangle", 12));
        shapeStorage.add(new Shape("Rhombus", 15));
        shapeStorage.add(new Shape("Circle", 20));
        shapeStorage.add(new Shape("Rectangle", 20));
        shapeStorage.add(new Shape("Trapeze", 20));
        // Метод T get(int index), который возвратит элемент по индексу в масиве;
        shapeStorage.getShape(1);
        // Метод T[] getAll(), который вернет массив элементов.Распечатать массив при помощи Arrays.toString;
        shapeStorage.getAll();
        // Метод, который подменит объект по заданной позиции только, если на этой позиции уже есть элемент;
        shapeStorage.update(1, "Square");
        // Метод, который удалит элемент по индексу и захлопнет пустую ячейку в массиве, если на этой позиции уже есть элемент;
        shapeStorage.delete(3);
        // Метод, который удалит заданный объект из массива;
        shapeStorage.delete("Triangle");
        // Метод который возвратит размер массива ЗАПОЛНЕНОГО;
        System.out.println(shapeStorage.mySize());

        GenericStorage<String, Shape<String,Integer>> shapeStorage2 = new GenericStorage(10);

        shapeStorage2.add(new Shape("Circle", 20));
        shapeStorage2.add(new Shape("Rectangle", 20));
        shapeStorage2.add(new Shape("Trapeze", 20));
        shapeStorage2.getAll();
    }
}
