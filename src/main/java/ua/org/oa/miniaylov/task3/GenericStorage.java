package ua.org.oa.miniaylov.task3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericStorage<T,S> {

        private T value1;
        private S value2;
        private int size;
        private  int counter = 0;


    public GenericStorage(){
        shapeStorage = new ArrayList<>();
    }

    public GenericStorage(int size){
    this.size = size;
    }

    List<Shape<T,S>> shapeStorage = new ArrayList<>(size);

    public boolean add(Shape shape) {
            counter++;
            return shapeStorage.add(shape);

    }

    public Shape<T, S> getShape(int index)throws IndexOutOfBoundsException{
        if(index <= shapeStorage.size() & index>=0 ) {
        return shapeStorage.get(index);
    }else{
            throw new IndexOutOfBoundsException ("Указаный индекс отсутствует в массиве");
        }
    }

    public T[] getAll(){
        List<T> shape = new ArrayList<>();
        for (Shape<T, S> tsShape : shapeStorage) {
            shape.add(tsShape.getTipe());
        }
        T [] shapes = (T[]) shape.toArray();
        System.out.println(Arrays.toString(shapes));
        return shapes;
    }

    public void update(int index, T tipe) throws IndexOutOfBoundsException{
        if(index <= shapeStorage.size() & index>=0 ){
            shapeStorage.set(index, new Shape<T,S>(tipe));
        }else{
            throw new IndexOutOfBoundsException ("Указаный индекс отсутствует в массиве");
        }
    }
    public void delete(int index)throws IndexOutOfBoundsException{
        if(index <= shapeStorage.size() & index>=0 ){
            shapeStorage.remove(index);
            counter--;
        }else{
            throw new IndexOutOfBoundsException("В указанной позиции нет элемента");
        }
    }
    public void delete(T tipe){
        for (int i = 0; i < shapeStorage.size(); i++) {
            if (shapeStorage.get(i).getTipe() == tipe){
                shapeStorage.remove(i);
                counter--;
            }
        }
    }
    public int  mySize(){
        return shapeStorage.size();
    }
    public T getValue1() {
        return value1;
    }

    public void setValue1(T value1) {
        this.value1 = value1;
    }

    public S getValue2() {
        return value2;
    }

    public void setValue2(S value2) {
        this.value2 = value2;
    }



    @Override
    public String toString() {
        return "GenericStorage{" +
                "value1=" + value1 +
                ", value2=" + value2 +
                ", humanStorages=" + shapeStorage +
                '}';
    }
}
