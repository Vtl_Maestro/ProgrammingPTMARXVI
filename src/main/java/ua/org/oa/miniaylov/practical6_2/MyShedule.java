package ua.org.oa.miniaylov.practical6_2;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Vitaliy on 31.05.2016.
 */
public class MyShedule {

    public void printMassages(LinkedHashMap<String, Integer> linkedHashMap){
        for(Map.Entry entry : linkedHashMap.entrySet()){
            System.out.println(entry.getValue());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(entry.getKey());
        }
    }
}
