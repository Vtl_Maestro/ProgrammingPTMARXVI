package ua.org.oa.miniaylov.practical6_2;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Vitaliy on 31.05.2016.
 */
public class App {

    public static void main(String[] args) {

        Map<String, Integer> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("Compilation completed successfully in 3s 45ms", 1745);
        linkedHashMap.put("Compilation completed successfully in 2s 33ms", 1751);
        linkedHashMap.put("Compilation completed successfully in 2s 48ms", 1755);
        linkedHashMap.put("Compilation completed successfully in 2s 47ms", 1759);
        linkedHashMap.put("Compilation completed successfully in 2s 44ms", 1764);
        linkedHashMap.put("Compilation completed successfully in 2s 41ms", 1767);
        linkedHashMap.put("Compilation completed successfully in 2s 42ms", 1771);

        MyShedule myShedule = new MyShedule();
        myShedule.printMassages((LinkedHashMap<String, Integer>) linkedHashMap);
    }
}
