package ua.org.oa.miniaylov.task5;

import java.io.*;
import java.util.*;

/**
 * Created by v_minjajlo on 25.05.2016.
 */
public class Dictionaries {

    Map<String, String> stringDictHashMap = new HashMap<>();
    List<String> stringDictList = new ArrayList<>();

    public Dictionaries() {
    }

    public void translates(String path, int dict) {
        buffReadDict(dict);
        buffReadWritDict(path);
    }

    public boolean checkDict(int dict) {
        if (dict > 0 && dict < stringDictList.size()+1) {
            return true;
        }
        return false;
    }

    public void listDiction() {
        File file = new File("C:\\Users\\v_minjajlo\\IdeaProjects\\Programming_20.05.2016_PTMARXVI\\src\\main\\resources\\dictionaries");
        if (file.exists()) {
            int i = 1;
            File[] fileNames = file.listFiles();
            stringDictList.clear();
            for (File fileName : fileNames) {
                System.out.println(i++ + " - " + fileName.getName());
                stringDictList.add(fileName.toString());
            }
        } else {
            System.out.println("Словарь не найден! Добавьте словарь в каталог: C:\\Users\\v_minjajlo\\IdeaProjects\\Programming_20.05.2016_PTMARXVI\\src\\main\\resources\\dictionaries");
       }
    }

    public void buffReadDict(int Dictionary) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(stringDictList.get(Dictionary - 1)), "UTF-8"))) {
            String s = null;
            stringDictHashMap.clear();
            while ((s = br.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(s, " ");
                stringDictHashMap.put(token.nextToken(), token.nextToken());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void buffReadWritDict(String path) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"))) {
            String s1 = null;
            String s2 = null;
            StringBuilder intext = new StringBuilder();
            StringBuilder outtext = new StringBuilder();
            while ((s1 = br.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(s1, " ");
                while (token.hasMoreTokens()) {
                    s2 = token.nextToken().toString();
                    if (stringDictHashMap.containsKey(s2)) {
                        outtext.append(stringDictHashMap.get(s2) + " ");
                    } else {
                        outtext.append(s2 + " ");
                    }
                }
                outtext.append("\n");
                intext.append(s1 + "\n");
            }
            System.out.println("Текст для перевода:");
            System.out.println(intext.toString());
            System.out.println("Переведенный текст:");
            System.out.println(outtext.toString());

        } catch (IOException e) {
            System.out.println("Не удается найти указанный файл");
            System.out.println("Проверьте путь к файлу");
        }
    }
}
