package ua.org.oa.miniaylov.task5;

import java.util.Scanner;

/**
 * Created by v_minjajlo on 25.05.2016.
 */
public class Consol {

    Dictionaries dictionaries = new Dictionaries();

    public Consol() {
        System.out.println("Это переводчик!");
    }

    public void consolDiction() {
        while (true) {
            Scanner in = new Scanner(System.in);
            System.out.println("Для зарешения работы введите \"exit\"");
            System.out.println("============================");
            System.out.println("Выберите словарь - введите цифру");
            dictionaries.listDiction();
            System.out.println("============================");
            String inputDictionary = in.nextLine();
            if (inputDictionary.equals("exit")) {
                break;
            }
            int inputDictionaryInt = Integer.parseInt(inputDictionary);

            if (dictionaries.checkDict(inputDictionaryInt) == false) {
                System.out.println("Введен не верный номер словаря!");
            } else {

                System.out.println("=============================");
                System.out.println("Укажите каталог:");
                String inputPass = in.nextLine();
                if (inputPass.equals("exit")) {
                    break;
                }
                System.out.println("=============================");
                System.out.println();
                dictionaries.translates(inputPass, inputDictionaryInt);
                System.out.println("Для выхода нажмите \"exit\"");
                String choice = in.nextLine();
                if (choice.equals("exit")) {
                    break;
                }
            }
        }
    }
}
