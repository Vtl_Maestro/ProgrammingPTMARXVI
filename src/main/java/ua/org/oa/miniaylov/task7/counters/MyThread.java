package ua.org.oa.miniaylov.task7.counters;

/**
 * Created by v_minjajlo on 22.06.2016.
 */
public class MyThread extends Thread {

    private boolean bool;

    public MyThread(String name){
        super(name);
    }

    @Override
    public void run() {

        while (Counters.count1 < 15) {
            synchronized (Counters.class) {
                bool = Counters.count1 == Counters.count2;
                System.out.println(bool);
                Counters.count1++;
                System.out.println(Counters.count1);

                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Counters.count2++;
                System.out.println(Counters.count2);
            }
        }
    }

}
