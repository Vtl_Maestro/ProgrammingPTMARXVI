package ua.org.oa.miniaylov.task7.deadlock;

/**
 * Created by v_minjajlo on 22.06.2016.
 */
public class App {

    public static void main(String[] args) {

        MailServer mailServer1 = new MailServer("ukr.net", new Mailbox("123@ukr.net"));
        MailServer mailServer2 = new MailServer("gmail.com", new Mailbox("456@gmail.com"));

        mailServer1.setRecipient(mailServer2);
        mailServer2.setRecipient(mailServer1);

        mailServer1.start();
        mailServer2.start();
    }
}
