package ua.org.oa.miniaylov.task7.deadlock;

/**
 * Created by v_minjajlo on 22.06.2016.
 */
public class Mailbox {

    private String address;

    public Mailbox(String address) {
        this.address = address;
    }

    public synchronized void sendLetter(MailServer mailServer){
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mailServer.getMailbox().receiveLetter(address);

    }

    public synchronized void receiveLetter(String address){
        System.out.println("Letter from" + address);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
