package ua.org.oa.miniaylov.task7.deadlock;

/**
 * Created by v_minjajlo on 22.06.2016.
 */
public class MailServer extends Thread{

    private String domain;
    private Mailbox mailbox;
    private MailServer recipient;

    public MailServer(String domain, Mailbox mailbox){
        setDomain(domain);
        setMailbox(mailbox);
    }


    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Mailbox getMailbox() {
        return mailbox;
    }

    public void setMailbox(Mailbox mailbox) {
        this.mailbox = mailbox;
    }

    public MailServer getRecipient() {
        return recipient;
    }

    public void setRecipient(MailServer recipient) {
        this.recipient = recipient;
    }

    @Override
    public void run() {
        mailbox.sendLetter(recipient);
    }
}
