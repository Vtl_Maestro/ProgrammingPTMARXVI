package ua.org.oa.miniaylov.task7.print_current_time;

import java.util.Scanner;

/**
 * Created by v_minjajlo on 21.06.2016.
 */
public class AppMyRunnable {

        public static void main (String[]args){
        System.out.println("Для запуска нажмите Enter");
        System.out.println("Для выхода нажмите q -> Enter");

        while (true) {
            Thread runnableThread = new Thread(new MyRunnable());
            Scanner in = new Scanner(System.in);
            String inputString = in.nextLine();
            runnableThread.setDaemon(true);
            runnableThread.start();
            if (inputString.equals("q")) {
                break;
            }
        }
    }
}

