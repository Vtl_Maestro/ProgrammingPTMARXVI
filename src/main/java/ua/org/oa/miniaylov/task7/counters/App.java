package ua.org.oa.miniaylov.task7.counters;

/**
 * Created by v_minjajlo on 22.06.2016.
 */
public class App {

    public static void main(String[] args) {

            Thread myThread1 = new MyThread("My Thread 1");
            Thread myThread2 = new MyThread("My Thread 2");
            Thread myThread3 = new MyThread("My Thread 3");
            myThread1.start();
            myThread2.start();
            myThread3.start();

    }
}
