package ua.org.oa.miniaylov.task7.print_current_time;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by v_minjajlo on 22.06.2016.
 */
public class PrintingTime extends Thread {

        private boolean terminated = false;

    public void terminate() {
        terminated = true;
        interrupt();
    }

    @Override
    public void run() {
        while (!terminated) {
            Date currentDate = new Date();
            System.out.println(DateFormat.getTimeInstance().format(currentDate));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
            }
        }
    }
}



