package ua.org.oa.miniaylov.task7.print_current_time;

import java.io.IOException;

/**
 * Created by v_minjajlo on 22.06.2016.
 */
public class AppPrintingTime {

    public static void main(String[] args) {

        System.out.println("Для выхода нажмте Enter");
        PrintingTime printingTime = new PrintingTime();
        printingTime.start();
        try {
            System.in.read();
        } catch (IOException ex) {
            System.out.println(ex);
        }
        printingTime.terminate();
    }
}
