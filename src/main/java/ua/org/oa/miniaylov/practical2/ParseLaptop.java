package ua.org.oa.miniaylov.practical2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseLaptop {

        public StringBuilder readerLpt() throws IOException {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("source"),"windows-1251"));
            String s = null;
            StringBuilder sb = new StringBuilder();
            while ((s = br.readLine()) != null) {
                sb.append(s).append(System.getProperty("line.separator"));
            }
            return sb;
        }

        public void parseLpt() throws IOException {
            String regex = "<span class=\"price cost\">(?<prise>\\d+\\s+.+?)<\\/span>.+\"lightbox\" title=(?<name>[\"()A-z0-9А-я- ]+) class.+<p class=\"description\">(?<description>.*?\\n*.*?)<br \\/>";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(readerLpt());
            while(matcher.find()){
                List<Laptop> listLaptop = new ArrayList();
                listLaptop.add(new Laptop(matcher.group("name"), matcher.group("prise"), matcher.group("description")));
                System.out.println(listLaptop.toString());
            }
        }
}
