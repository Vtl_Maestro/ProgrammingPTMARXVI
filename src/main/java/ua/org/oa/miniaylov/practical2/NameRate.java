package ua.org.oa.miniaylov.practical2;


public class NameRate {

    private String maleName;
    private String femaleName;
    private String rating;

    public NameRate(){
    }

    public NameRate(String rating, String maleName, String femaleName){
        setMaleName(maleName);
        setFemaleName(femaleName);
        setRating(rating);
    }
    public String getMaleName() {
        return maleName;
    }

    public void setMaleName(String maleName) {
        this.maleName = maleName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getFemaleName() {
        return femaleName;
    }

    public void setFemaleName(String femaleName) {
        this.femaleName = femaleName;
    }

    @Override
    public String toString() {
        return "NameRate{" +
                "maleName: '" + maleName + '\'' +
                ", femaleName: '" + femaleName + '\'' +
                ", rating: " + rating +
                '}';
    }
}
