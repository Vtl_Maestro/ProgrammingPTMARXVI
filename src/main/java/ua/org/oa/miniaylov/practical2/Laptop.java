package ua.org.oa.miniaylov.practical2;


public class Laptop {

    private String name;
    private String prise;
    private String description;

    public Laptop(){

    }

    public Laptop(String name, String prise, String description) {
        setName(name);
        setPrise(prise);
        setDescription(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrise() {
        return prise;
    }

    public void setPrise(String prise) {
        this.prise = prise;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "name: '" + name + '\'' +
                ", prise: '" + prise + '\'' +
                ", description: '" + description + '\'' +
                '}';
    }
}
