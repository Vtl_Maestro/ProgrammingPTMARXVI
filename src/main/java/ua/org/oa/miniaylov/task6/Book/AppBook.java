package ua.org.oa.miniaylov.task6.Book;

/**
 * Created by v_minjajlo on 24.05.2016.
 */
public class AppBook {

    public static void main(String[] args) {

        UtilBook utilBook = new UtilBook();
        utilBook.readBook("C:\\Users\\v_minjajlo\\IdeaProjects\\Programming_20.05.2016_PTMARXVI\\src\\main\\resources\\listBook.txt");
        System.out.println("Прочитанный список из файла:");
        utilBook.printList();
        utilBook.serializeBook(utilBook.getBookList(), "C:\\Users\\v_minjajlo\\IdeaProjects\\Programming_20.05.2016_PTMARXVI\\src\\main\\resources\\bookSerial.txt");
        System.out.println("Десерилизованный объект: " + utilBook.deserializeBook("C:\\Users\\v_minjajlo\\IdeaProjects\\Programming_20.05.2016_PTMARXVI\\src\\main\\resources\\bookSerial.txt"));

    }
}
