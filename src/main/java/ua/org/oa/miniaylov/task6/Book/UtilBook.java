package ua.org.oa.miniaylov.task6.Book;

import ua.org.oa.miniaylov.task6.Book.Book;

import java.io.*;
import java.util.*;

/**
 * Created by v_minjajlo on 24.05.2016.
 */
public class UtilBook implements Serializable {

    private List<Book> bookList = new ArrayList<>();
    public UtilBook() {
    }

    public List readBook(String path){
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"))) {
            String s = null;

            while ((s = br.readLine()) != null) {
                Book book = new Book();
                StringTokenizer token = new StringTokenizer(s, ";");
//                StringBuilder sb = new StringBuilder();
//                sb.append(s).append("\n");
//                book.setAuthor(sb.append());
                book.setAuthor(token.nextToken());
                book.setTitle(token.nextToken());
                book.setYear(Integer.parseInt(token.nextToken()));
                addList(book);
            }
        } catch (IOException e) {
            System.out.println("Не удается найти указанный файл");
        }
        return bookList;
    }
    private void addList(Book book){
        bookList.add(book);
    }
    public void serializeBook(List bookListSer , String path){
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))){
                oos.writeObject(bookListSer);

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List deserializeBook(String path){
        List listBookDeser = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))){
            listBookDeser = (List) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return bookList;
    }

    public void printList(){
    for (Book book1 : bookList) {
        System.out.println(book1);
       }
    }

    public List<Book> getBookList() {
        return bookList;
    }
}
