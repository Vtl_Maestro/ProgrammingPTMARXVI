package ua.org.oa.miniaylov.task6.FileManager;

import java.util.Scanner;

/**
 * Created by v_minjajlo on 24.05.2016.
 */
public class CommandFileManager {

    UtilFileManager ufm = new UtilFileManager();

    public CommandFileManager() {
        System.out.println("Это FileManager");
    }
    public void commandFileManager() {
        while (true) {
            Scanner in = new Scanner(System.in);
            System.out.println("Для выхода нажмите \"exit\"");
            System.out.println("=============================");
            System.out.println("Список команд:\n create - создание файла\n delete - удаление файла\n rename - переименовать файл\n dir - список файлов");
            System.out.println("Введите команду:");
            String inputCommand = in.nextLine();
            if (inputCommand.equals("exit")) {
                break;
            }
            if (ufm.checkCommand(inputCommand)) {
                if (inputCommand.equals("rename")) {
                    System.out.println("Введите имя файла, который хотите переименоавать!");
                    String oldFileName = in.nextLine();
                    if (oldFileName.equals("exit")) {
                        break;
                    }
                    System.out.println("Введите новое имя и директорию файла:");
                    String newFileName = in.nextLine();
                    if (newFileName.equals("exit")) {
                        break;
                    }
                    ufm.renameFile(oldFileName, newFileName);
                } else {
                    System.out.println("Введите имя и директорию файла:");
                    String inputPass = in.nextLine();
                    if (inputPass.equals("exit")) {
                        break;
                    }
                    switch (inputCommand){
                        case "create":
                            ufm.createFile(inputPass);
                            break;
                        case "dir":
                            ufm.dir(inputPass);
                            break;
                        case "delete":
                            ufm.deleteFile(inputPass);
                            break;
                    }
                }
            }else {
                System.out.println("Введена не верная команда, введите повторно!");
            }

        }
    }
}
