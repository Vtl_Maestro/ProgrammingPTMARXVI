package ua.org.oa.miniaylov.task6.Book;

import java.io.Serializable;
import java.util.List;

/**
 * Created by v_minjajlo on 24.05.2016.
 */
public class Book implements Serializable {
    private static final long serialVersionUID = -2103930769651100839L;
    private String title;
    private String author;
    private int year;

    public Book() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                "title='" + title + '\'' +
                ", year=" + year +
                '}';
    }
}
