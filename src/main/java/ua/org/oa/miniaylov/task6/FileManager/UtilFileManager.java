package ua.org.oa.miniaylov.task6.FileManager;

import java.io.File;
import java.io.IOException;

/**
 * Created by v_minjajlo on 24.05.2016.
 */
public class UtilFileManager {

    public UtilFileManager() {
    }

    public void createFile(String pass) {

        File file = new File(pass);
        if (file.exists()) {
            System.out.println("Файл с таким именем уже существует!");

        } else {
            try {
                file.createNewFile();
                System.out.println("Файл создан!");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteFile(String pass) {
        File file = new File(pass);
        if (file.exists()) {
            file.delete();
            System.out.println("Файл удален!");
        } else {
            System.out.println("Файл не найден!");
        }
    }

    public void renameFile(String oldName, String newName) {
        File oldFileName = new File(oldName);
        File newFileName = new File(newName);

        if(newFileName.exists()){
            System.out.println("Файл с таким именем уже существует!");
        }
        if (oldFileName.exists()) {
            oldFileName.renameTo(newFileName);
            System.out.println("Файл переименован!");
        }else{
            System.out.println("Файл не найден!");
        }
    }

    public void dir(String pass) {
        File file = new File(pass);
        if (file.exists()) {
            System.out.println("Найденные файлы: ");
            File[] fileNames = file.listFiles();
            for (File fileName : fileNames) {
                System.out.println(fileName.getName());
            }

        } else {
            System.out.println("Файл не найден!");
        }
    }

    public boolean checkCommand(String command) {
        String[] commands = {"rename", "delete", "create", "dir"};
        for (String s : commands) {
            if (s.equals(command)) {
                return true;
            }
        }
        return false;
    }
}

