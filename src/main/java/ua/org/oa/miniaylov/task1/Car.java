package ua.org.oa.miniaylov.task1;

public class Car {

    private int yearOfIssue;
    private String carModel;
    private int price;

    public Car(int yearOfIssue, String carModel, int price) {
        setYearOfIssue(yearOfIssue);
        setCarModel(carModel);
        setPrice(price);
    }

    public int getYearOfIssue() {
        return yearOfIssue;
    }

    public void setYearOfIssue(int yearOfIssue) {

        this.yearOfIssue = yearOfIssue;
    }

    public String getCarModel()
    {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "yearOfIssue=" + yearOfIssue +
                ", carModel='" + carModel + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (yearOfIssue != car.yearOfIssue) return false;
        if (price != car.price) return false;
        return carModel != null ? carModel.equals(car.carModel) : car.carModel == null;

    }

    @Override
    public int hashCode() {
        int result = yearOfIssue;
        result = 31 * result + (carModel != null ? carModel.hashCode() : 0);
        result = 31 * result + price;
        return result;
    }
}
