package ua.org.oa.miniaylov.task1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class App {

    public static void main(String[] args) {

        // Вычисление дня недели по дате:
        Date date = new Date(11, 04, 2016);
        date.dayOfWeek();
        date.getDayOfWeek();
        System.out.println("==========================");

        // Проверка на високосность и количество дней в месяце:
        Date.Year year = new Date(11, 04, 2016).new Year();
        year.leapYear();
        year.getDaysMonth();
        System.out.println("==========================");

        // Вычисление количества дней между датами:
        Date date1 = new Date(01, 01, 2016);
        Date date2 = new Date(01, 03, 2016);
        System.out.print("Количество дней, в заданном временном промежутке: ");
        System.out.println(date2.daysBetween(date2) - date1.daysBetween(date1));

        System.out.println("==========================");


        // Анонимный класс;
        Car car1 = new Car(2015, "Porshe", 70000) {

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Car car = (Car) o;
                if (getYearOfIssue() != car.getYearOfIssue()) return false;
                return getCarModel() != null ? getCarModel().equals(car.getCarModel()) : car.getCarModel() == null;
            }
            @Override
            public String toString() {
                return "Год выпуска: "+getYearOfIssue()+", "+"Марка: "+getCarModel();
            }

        };
        System.out.println("car: " + car1);


        Car car2 = new Car(2016, "BMW", 50000) {

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Car car = (Car) o;
                if (getPrice() != car.getPrice()) return false;
                return getCarModel() != null ? getCarModel().equals(car.getCarModel()) : car.getCarModel() == null;
            }

            @Override
            public String toString() {
                return "Цена: "+getPrice()+", "+"Марка: "+getCarModel();
            }
        };
        System.out.println("car2: " + car2);

    }

}