package ua.org.oa.miniaylov.task1;

public class Date {
    private int day, month, year, resultDayOfWeek;
    private boolean yearLeap;

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int dayOfWeek() {
        int result = 0;
        int a = (14 - month) / 12, y = year - a, m = month + 12 * a - 2;
        result = (7000 + (day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12)) % 7;
        this.resultDayOfWeek = result;
        return result;
    }

    public void getDayOfWeek() {

        switch (resultDayOfWeek) {
            case 0:
                System.out.println(Day.SUNDAY);
                break;
            case 1:
                System.out.println(Day.MONDEY);
                break;
            case 2:
                System.out.println(Day.TUESDAY);
                break;
            case 3:
                System.out.println(Day.WEDNESDAY);
                break;
            case 4:
                System.out.println(Day.THERSDAY);
                break;
            case 5:
                System.out.println(Day.FRIDAY);
                break;
            case 6:
                System.out.println(Day.SATURDAY);
                break;
        }
    }

    public int daysBetween(Date date) {
        int result = 0;
        int[] getDayFalseLeap = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int[] getDayTrueLeap = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        if (year % 4 != 0) {
            for (int i = 0; i < month; i++) {
                result = result + getDayFalseLeap[i];
            }
            result = result + day;
            return result;
        } else {
            for (int i = 0; i < month; i++) {
                result = result + getDayTrueLeap[i];
            }
            result = result + day;
            return result;
        }
    }

    class Year {

        public void leapYear() {
            if (year % 4 == 0) {
                System.out.println("Год високосный!");
                yearLeap = true;
            } else {
                System.out.println("Год не високосный!");
                yearLeap = false;
            }
        }

        public void getDaysMonth() {

            switch (month) {
                case 1:
                    System.out.println("Январь - 31 день");
                    break;
                case 2:
                    if (yearLeap == true) {
                        System.out.println("Февраль - 29 дней");
                    } else {
                        System.out.println("Февраль - 28 дней");
                    }
                    break;
                case 3:
                    System.out.println("Март - 31 день");
                    break;
                case 4:
                    System.out.println("Апрель - 30 дней");
                    break;
                case 5:
                    System.out.println("Май - 31 день");
                    break;
                case 6:
                    System.out.println("Июнь - 30 дней");
                    break;
                case 7:
                    System.out.println("Июль - 31 день");
                    break;
                case 8:
                    System.out.println("Август - 31 день");
                    break;
                case 9:
                    System.out.println("Сентябрь - 30 дней");
                    break;
                case 10:
                    System.out.println("Октябрь - 31 день");
                    break;
                case 11:
                    System.out.println("Ноябрь - 30 дней");
                    break;
                case 12:
                    System.out.println("Декабрь - 31 день");
                    break;
            }
        }
    }

    class Month {

//        public int[] getDayFalseLeap = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
//        public int[] getDayTrueLeap = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    }

    enum Day{

        SUNDAY(0), MONDEY(1), TUESDAY(2), WEDNESDAY(3), THERSDAY(4), FRIDAY(5), SATURDAY(6);
        Day( int index){
        }
    }
}
