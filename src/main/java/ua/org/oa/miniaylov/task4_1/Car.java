package ua.org.oa.miniaylov.task4_1;

public class Car {

    private String name;
    private int serialNumber;

    public Car(String name, int serialNumber) {
        setName(name);
        setSerialNumber(serialNumber);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", serialNumber=" + serialNumber +
                '}';
    }
}
