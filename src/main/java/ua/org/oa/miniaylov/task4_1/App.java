package ua.org.oa.miniaylov.task4_1;

public class App {
    public static void main(String [] args){

        Integer[] listInt = {1, 2, 3, 4, 5};
        Util.maxElement(listInt);
        String[] listString = {"Petrov", "Sidiriv", "Ivanov"};
        Util.maxElement(listString);
        Car[] cars = {new Car("BMW", 511), new Car("Subaru", 07), new Car("Opel", 45)};
        Util.maxElement(cars);
        Computer[] computers = {new Computer("IBM", 012345), new Computer("Mac", 45678978)};
        Util.maxElement(computers);
    }
}
