package ua.org.oa.miniaylov.task4_1;

public class Computer implements Comparable {

    private String model;
    private int number;


    public Computer(String model, int number) {
        setModel(model);
        setNumber(number);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }


    @Override
    public int compareTo(Object o) {
        return 0;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "model='" + model + '\'' +
                ", namber=" + number +
                '}';
    }
}
