package ua.org.oa.miniaylov.task4_1;

import java.util.Arrays;

public class Util{

//    public static <T extends Comparable> void  maxElement (T[] array) {
//        Arrays.sort (array, (o1, o2) -> {
//            return (o1).compareTo(o2);
//        });
//        System.out.println("maxElement = "+array[array.length-1]);
//    }


        public static <T> void  maxElement ( T[] array) {
            Arrays.sort (array, (o1, o2) -> {
                return ((Comparable)o1).compareTo((Comparable)o2);
            });
            System.out.println("maxElement = "+array[array.length-1]);
        }
}

