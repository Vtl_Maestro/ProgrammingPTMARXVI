package ua.org.oa.miniaylov.task2;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MarkdownParser {

    public MarkdownParser() {}

    public String parser(String originStr) {
        StringTokenizer st = new StringTokenizer(originStr, "\n");
        StringBuilder sb = new StringBuilder();
        while (st.hasMoreTokens()) {
            sb = sb.append(check(st.nextToken().toString()));
        }
        String result = "<html>\n" + "<body>\n" + sb.toString()+ "\n" + "</body>\n" + "</html>\n" ;
        return result;
    }

    public String check(String str) {
        int index;
        String newString = "";
        String regexForHead = "#*";
        Pattern pattern = Pattern.compile(regexForHead);
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            String count = matcher.group();
            index = count.length();
            if (index > 0) {
                String result = "<h" + index + ">" + str.substring(index) + "</h" + index + ">";
                return result;
            } break;
        }
        String regexEmph = "\\*\\*\\w*\\*\\*";
        pattern = Pattern.compile(regexEmph);
        matcher = pattern.matcher(str);
        while (matcher.find()) {
            String temp = matcher.group();
            str = str.replace(temp, "<em>" +temp.substring(2, temp.length()-2) + "</em>" );
            newString = str;
        }
        String regexStrong = "\\*\\w*\\*";
        pattern = Pattern.compile(regexStrong);
        matcher = pattern.matcher(str);
        while (matcher.find()) {
            String temp = matcher.group();
            str = str.replace(temp, "<strong>" +temp.substring(1, temp.length()-1) + "</strong>" );
            newString = str;
        }
        String regexLink = "(?<description>\\[[\\w\\s]*\\])(?<link>\\([\\w\\:\\/\\.]*\\))";
        pattern = Pattern.compile(regexLink);
        if (newString.length() > 0){
            matcher = pattern.matcher(newString);
        }else{
            matcher = pattern.matcher(str);
        }
        while (matcher.find()) {
            String temp = matcher.group("description");
            String temp2 = matcher.group("link");
            newString = matcher.replaceAll("<a href=\""+ temp2.substring(1, temp2.length()-1) + "\">" + temp.substring(1, temp.length()-1) + "</a>" );
        }
        return "\n<p>" + newString+"</p>";
    }
}