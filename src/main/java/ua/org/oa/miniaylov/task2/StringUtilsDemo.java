package ua.org.oa.miniaylov.task2;


public class StringUtilsDemo {

    public static void main(String[] args) {

        StringUtils stringUtils = new StringUtils();
        System.out.println(stringUtils.overturn("!dlrow olleH"));
        System.out.println(stringUtils.isPalindrome("А роза упала на лапу Азора"));
        stringUtils.checkString("1234567891");
        stringUtils.checkString("1234567");
        System.out.println(stringUtils.lastFirst("были зашифрованы на старые сертификаты"));
        stringUtils.sentenceLastFirst("приходит только Маша.уходит только Петя. ");
        System.out.println(stringUtils.isAbc("abcabcabc"));
        System.out.println(stringUtils.isDate("12.12.2015"));
        System.out.println(stringUtils.isMail("fgh123@gmail.com"));
        stringUtils.isTel("During the George Bush Administration, +1(360)123-45-45 the neoconservatives.Their fervor caused not only thousands of Americans’ deaths +1(360)456-70-80 but also hundreds fortunate countries. ");
    }
}
