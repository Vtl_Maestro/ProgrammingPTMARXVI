package ua.org.oa.miniaylov.task2;

public class DemoMarkdown {

    public static void main(String[] args) {

        MarkdownParser markdownParser = new MarkdownParser();

        String s = new String("##Header line\n" +
                "Simple line **with** em\n" +
                "Simple *line* with strong\n" +
                "Line with link [Link to google](https://www.google.com) in center\n" +
                "Line *with* many *elements* and link [Link to FB](https://www.facebook.com)");

        System.out.println(markdownParser.parser(s));

    }
}
