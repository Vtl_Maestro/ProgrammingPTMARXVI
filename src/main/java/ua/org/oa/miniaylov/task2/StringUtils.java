package ua.org.oa.miniaylov.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    // функция, которая переворачивает строчку наоборот. Пример: было “Hello world!” стало “!dlrow olleH”
    public String overturn(String string){
        StringBuilder sb = new StringBuilder(string);
        return sb.reverse().toString();
    }
    // функция, которая определяет является ли строчка полиндромом. Пример: А роза упала на лапу Азора
    public boolean isPalindrome(String string){
        string = string.replace(" ","");
        string = string.toLowerCase();
        String reverseStr = overturn(string);
        if (string.equals(reverseStr)){
            return true;
        }else{
            return false;
        }
    }

    // функция которая проверяет длину строки, и если ее длина больше 10, то оставить в строке только первые 6 символов, иначе дополнить строку символами 'o' до длины 12.
    public String checkString(String string){
        String str = null;
        if(string.length() >= 10){
        StringBuilder sb = new StringBuilder(string);
           sb.delete(6, sb.length());
            str = sb.toString();
            System.out.println(str);
        }else{
            if((string.length() < 10)){
                int in = 12-string.length();
                char [] chars = new char[in];
                StringBuilder sb = new StringBuilder(string);
                for (int i = 0; i < chars.length ; i++) {
                    chars[i] = 'o';
                }
                sb.append(chars);
                str = sb.toString();
                System.out.println(str);
            }
        }
        return str;
    }
    // функция, которая меняет местами первое и последнее слово в строчке
    public String lastFirst(String string){
        String [] stringArr = string.split(" ");
        String str = "";
            String first = stringArr[0];
            stringArr[0] = stringArr[stringArr.length-1];
            stringArr[stringArr.length-1] = first;
        for (String s : stringArr) {
            str = str.concat(s.toString() + " ");
        }
        return str;
    }

    // функция, которая меняет местами первое и последнее слово в каждом предложении. (предложения могут разделятся ТОЛЬКО знаком точки)
    public String sentenceLastFirst(String string){
        String result = "";
        String[] strArr = string.split("[\\.]");
        for (int i = 0; i < strArr.length-1; i++) {
            System.out.println("Входящая строка: "+strArr[i]);
        }
        for (int i = 0; i < strArr.length-1; i++) {
            result += lastFirst(strArr[i]);
        }
        System.out.println("Результат: "+result);
        return result;
    }
    // функция, которая проверяет содержит ли строка только символы 'a', 'b', 'c' или нет.
    public boolean isAbc(String string){
        String regex = "[a-c]+";
        return Pattern.matches(regex, string);

    }
    // функция, которая определят является ли строчка датой формата MM.DD.YYYY
    public boolean isDate(String string) {
        Pattern pattern = Pattern.compile("\\b(\\d{2}\\.\\d{2}\\.\\d{4})");
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
    // функция, которая определяет является ли строчка почтовым адресом
    public boolean isMail(String string){
        Pattern pattern = Pattern.compile("\\b(\\w+@\\w+\\.\\w+)");
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }
    // функция, которая находит все телефоны в переданном тексте формата +Х(ХХХ)ХХХ-ХХ-ХХ, и возвращает их в виде массива
    public String[] isTel(String string) {
        List<String> listString = new ArrayList();
        Pattern pattern = Pattern.compile("(?<tel>\\+\\d\\(\\d{3}\\)\\d{3}\\-\\d{2}\\-\\d{2})");
//        Pattern pattern = Pattern.compile("(\\+\\d\\(\\d{3}\\)\\d{3}\\-\\d{2}\\-\\d{2})");
        Matcher matcher = pattern.matcher(string);
        while (matcher.find()) {
            listString.add(new String(matcher.group("tel")));
        }
        String[] stringArr = new String[listString.size()];
        for (int i = 0; i < listString.size(); i++) {
            stringArr[i] = listString.get(i);
            System.out.println(stringArr[i]);
        }
        return listString.toArray(new String[listString.size()]);
    }
}



















