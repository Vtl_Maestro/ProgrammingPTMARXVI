package ua.org.oa.miniaylov.practical1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by v_minjajlo on 05.04.2016.
 */
public class ArraySumTest{

    @Test
    public void sum() {
       int actual = ArraySum.sum(new int[]{1,2,3});
        int expected = 6;
        Assert.assertEquals("Test for ArraySum",expected,actual);
    }

    @Test(expected = NullPointerException.class)
    public void sumNullPointerException() {

        int actual = ArraySum.sum(null);
        Class<NullPointerException> expected = NullPointerException.class;
        Assert.assertEquals("Test for ArraySum_2", expected, actual);


    }

}