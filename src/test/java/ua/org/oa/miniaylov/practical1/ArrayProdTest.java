package ua.org.oa.miniaylov.practical1;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Vitaliy on 20.06.2016.
 */
public class ArrayProdTest {

    private double actual;
    private double expected;

    @Before
    public void setUp(){
        ArrayProd arrayProd = new ArrayProd(new double[]{1,2,3});
        this.expected = 6;
        this.actual = arrayProd.prod();
    }

    @Test
    public void prod(){
        Assert.assertEquals("Test for ArrayProd", expected, actual, 0.001);

    }

}