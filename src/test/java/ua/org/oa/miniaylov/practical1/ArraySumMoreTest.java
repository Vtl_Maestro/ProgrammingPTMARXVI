package ua.org.oa.miniaylov.practical1;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 * Created by v_minjajlo on 05.04.2016.
 */
public class ArraySumMoreTest extends ArraySumTest {
    private double expected;
    private double actual;

    @Before
    public void setUp()  {
        this.actual = ArraySum.sum(new int[]{1,2,3});
        this.expected = 6;
    }

    @Test
    public void sum() {
        Assert.assertEquals("Test for ArraySum",expected,actual,0.001);
    }
}