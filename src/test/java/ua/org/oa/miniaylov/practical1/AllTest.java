package ua.org.oa.miniaylov.practical1;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Vitaliy on 20.06.2016.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({ArraySumTest.class, ArraySumMoreTest.class, ArrayProdTest.class})

public class AllTest {

}