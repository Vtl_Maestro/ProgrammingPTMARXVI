package ua.org.oa.miniaylov.task2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Vitaliy on 20.06.2016.
 */

public class StringUtilsTest {

    StringUtils set;

    @Before
    public void setUp(){
        set = new StringUtils();
    }

    @Test
    public void overturn(){
        String actual = set.overturn("!dlrow olleH");
        String expected = "Hello world!";
        Assert.assertEquals("Test for overturn", expected, actual);
    }

    @Test
    public void isPalindrome(){
        Boolean actual = true;
        Assert.assertEquals("Test for overturn", set.isPalindrome("А роза упала на лапу Азора"), actual);
    }

    @Test
    public void checkString(){
        String actual = set.checkString("123456789123");
        String expected = "123456";
        Assert.assertEquals("Test for checkString if > 10", expected, actual);
        actual = set.checkString("123456789");
        expected = "123456789ooo";
        Assert.assertEquals("Test for checkString if < 10", expected, actual);
    }

    @Test
    public void lastFirst(){
        String actual = set.lastFirst("были зашифрованы на старые сертификаты");
        String expected = "сертификаты зашифрованы на старые были ";
        Assert.assertEquals("Test for lastFirst", expected, actual);
    }

    @Test
    public void sentenceLastFirst() {
        String actual = set.sentenceLastFirst("приходит только Маша.уходит только Петя. ");
        String expected = "Маша только приходит Петя только уходит ";
        Assert.assertEquals("Test for lastFirst", expected, actual);
    }

    @Test
    public void isAbc(){
        String actual = "abcbcaaccabca";
        assertTrue("isAbc", set.isAbc(actual));
        actual = "aaaaaaaaawww";
        assertFalse("notAbc", set.isAbc(actual));

    }

    @Test
    public void isDate(){
        String actual = "srtfgsrt456";
        assertFalse("notDate", set.isDate(actual));
        actual = "01.07.2016";
        assertTrue("isDate", set.isDate(actual));
    }

    @Test
    public void isMail(){
        String actual = "qwer123@qwer123.com";
        assertTrue("isMail", set.isMail(actual));
        actual = "xxx.xx.com";
        assertFalse("notMail", set.isMail(actual));
    }

    @Test
    public void isTel(){
        String [] actual = set.isTel("During the George Bush Administration, +1(360)123-45-45 the neoconservatives.Their fervor caused not only thousands of Americans’ deaths +1(360)456-70-80 but also hundreds fortunate countries.");
        String [] expected = {"+1(360)123-45-45", "+1(360)456-70-80"};
        assertEquals(expected, actual);
    }
}